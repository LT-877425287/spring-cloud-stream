package com.tao.rocketstream.tx;

import com.tao.rocketstream.producer.MySource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;
import org.springframework.util.MimeTypeUtils;

/**
 * @author Administrator
 * @title: Sender
 * @projectName SpringCloudStream
 * @description: TODO
 * @date 2021/1/1918:22
 */

@Component
public class Sender {

    @Autowired
    private MySource mySource;

    public <T> void sendTransactionalMsg(T msg,int num){
        MessageBuilder builder = MessageBuilder.withPayload(msg)
                .setHeader(MessageHeaders.CONTENT_TYPE, MimeTypeUtils.APPLICATION_JSON)
                .setHeader("test", String.valueOf(num));
        Message message=builder.build();
        mySource.outputTX().send(message);
    }

}
