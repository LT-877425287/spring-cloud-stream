package com.tao.rocketstream;

import com.tao.rocketstream.consumer.MySink;
import com.tao.rocketstream.consumer.ReceiveService;
import com.tao.rocketstream.producer.MySource;
import com.tao.rocketstream.producer.SenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.cloud.stream.messaging.Sink;


@EnableBinding(value = {Source.class, Sink.class, MySource.class, MySink.class})
@SpringBootApplication
public class RocketStreamApplication/* implements CommandLineRunner*/ {


   /* @Autowired
    private SenderService senderService;

    @Autowired
    private ReceiveService receiveService;*/

    public static void main(String[] args) {
        SpringApplication.run(RocketStreamApplication.class, args);
    }

    /*public void run(String... args) throws Exception {


        senderService.send1("hello");

        //receiveService.receive();
    }*/

}
