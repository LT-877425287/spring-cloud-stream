package com.tao.rocketstream.producer;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

/**
 * @author Administrator
 * @title: MySourve
 * @projectName SpringCloudStream
 * @description: TODO
 * @date 2021/1/1917:48
 */
public interface MySource {

    String OUTPUT = "output1";

    @Output(MySource.OUTPUT)
    MessageChannel output1();

    String OUTPUT2 = "output2";

    @Output(MySource.OUTPUT2)
    MessageChannel output2();

    String OUTPUTTX = "outputTX";

    @Output(MySource.OUTPUTTX)
    MessageChannel outputTX();

}
