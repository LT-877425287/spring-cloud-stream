package com.tao.rocketstream.consumer;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

/**
 * @author Administrator
 * @title: MySink
 * @projectName SpringCloudStream
 * @description: TODO
 * @date 2021/1/1918:02
 */
public interface MySink {

    String INPUT = "input1";

    @Input(MySink.INPUT)
    SubscribableChannel input1();

    String INPUT2 = "input2";

    @Input(MySink.INPUT2)
    SubscribableChannel input2();


    String INPUTTX = "inputTX";

    @Input(MySink.INPUTTX)
    SubscribableChannel inputTX();
}
