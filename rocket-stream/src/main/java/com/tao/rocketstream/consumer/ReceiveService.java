package com.tao.rocketstream.consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;

import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;

/**
 * @author Administrator
 * @title: ReceiveService
 * @projectName SpringCloudStream
 * @description: TODO
 * @date 2021/1/1914:54
 */

@Service
public class ReceiveService {

    @Autowired
    private Sink sink;

    /*public void receive(){
        sink.input().subscribe((Message<?> message)->{
            System.out.println(message.getPayload());
        });
    }*/

    @StreamListener(value = Sink.INPUT)
    public void receiveMessage(String message){
        System.out.println("接收的消息input==>"+message);
    }

    @StreamListener(value = MySink.INPUT)
    public void receiveMessage1(String message){
        System.out.println("接收的消息input1==>"+message);
    }

    @StreamListener(value = MySink.INPUT2)
    public void receiveMessage2(String message){
        System.out.println("接收的消息input2==>"+message);
    }

    @StreamListener(value = MySink.INPUTTX)
    public void receiveMessageTX(String message){
        System.out.println("接收的消息inputTX==>"+message);
    }

}
